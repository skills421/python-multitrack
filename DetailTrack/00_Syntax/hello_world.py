"""
this is a module for printing Hello World
it contains one command - the print command
"""

# this is a call to print the text Hello World
print('Hello World')

# this is a call to refer to the value 20 using the name age
age = 20

# this is a logic block
# all the indented lines apply to the first non-indented line
# in this case if the age is greater than or equal to 21
# both the prints will execute

# note that the indentation level must be consistent throughout the
# block of code
if age >= 21:
    print("You are", age)
    print('Old enough to drink in the USA')
