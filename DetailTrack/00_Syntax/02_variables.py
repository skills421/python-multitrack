# variables are names we use
# to refer to objects (values) in memory

25
'Hello'
#  we can't access these objects unless
# we refer to them with a name

age = 25
msg = 'Hello'
# now we can refer to the objects
# by name

print(age, msg) # print2: 25 'Hello

# A variable name must start with a letter or the underscore character
# A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
# Variable names are case-sensitive (msg, Msg and MSG are three different variables)

# more than one variable can be assigned
# at a time
name, age, height, weight = 'Jon Doe', 21, 1.85, 140
print(name, age, height, weight)

# prints: Jon Doe 21 1.85 140

# multiple variables can be assigned to the same value
x = y = z = 0
print(x, y, z)
# prints: 0 0 0


