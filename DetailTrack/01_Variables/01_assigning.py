
"""
* must start with a letter or the underscore character
* cannot start with a number
* can only contain alpha-numeric characters and underscores
"""

first_name = 'Jon'
last_name = 'Doe'
age = 21
height = 1.85

print(first_name)
print(last_name)
print(age)
print(height)

print(type(first_name))
print(type(last_name))
print(type(age))
print(type(height))

house = 18
street = 'Main Street'
city = 'London'
postcode = 'SW1A 1AA'

print(house)
print(street)
print(city)
print(postcode)

house = "The Laurels"

print(house)
