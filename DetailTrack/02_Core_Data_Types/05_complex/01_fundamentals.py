"""
complex numbers are written with a 'j' as the imaginary part.
"""

x = 2 + 4j
y = 5j
z = -2j

print(x)
print(x.real)
print(x.imag)

print(y)
print(y.real)
print(y.imag)

print(z)
print(z.real)
print(z.imag)
print()

x = 3
j = 4

z = complex(x, j)
print(z)
print(type(z))