"""
The character e or E can be appended, followed by a positive or negative number to specify scientific notation

3.142e0  = 3.142 * 10^0
3.142e1  = 3.142 * 10^1
3.142e-1 = 3.142 * 10^-1 = 3.142 / 10^1

Most platforms represent Python floats as 64-bit "doubles"
The max value a floating point number can have is approx 1.8 * 10^308
Python will represent this as 'inf' or 'infinity'

The min non-zero value a floating point number can have is approx 5.0 * 10^-324
"""

x = 3.142
print(f"3.142:        {x}")

x = 3.142e0
print(f"3.142e0:      {x}")

x = 3.142e1
print(f"3.142e1:      {x}")

x = 3.142e2
print(f"3.142e2:      {x}")

x = 3.142e3
print(f"3.142e3:      {x}")

print()

x = 3.142
print(f"3.142:        {x}")

x = 3.142e0
print(f"3.142e0:      {x}")

x = 3.142e-1
print(f"3.142e-1:     {x}")

x = 3.142e-2
print(f"3.142e-2:     {x}")

print()

##

print(f"1.79e308:     {1.79e308}")
print(f"1.80e308:     {1.80e308}")

print(f"5.00e-324:     {5.00e-324}")
print(f"5.00e-325:     {5.00e-325}")