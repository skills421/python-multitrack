"""
Float, or 'floating point number' is a number, positive or negative, containing one or more decimals.
"""

pi = 3.142
x = 1.0
y = 0.98
z = -2.4

print(pi)
print(x)
print(y)
print(z)

print(type(pi))
print(type(x))
print(type(y))
print(type(z))