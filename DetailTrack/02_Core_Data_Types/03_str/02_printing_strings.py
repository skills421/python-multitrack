first_name = 'Jon'
last_name = 'Doe'
age = 21
height = 1.84

# Print raw values
print(first_name)
print(last_name)
print(age)
print(height)
print()


# Concatenating Strings and ints
print(first_name + last_name + str(age) + str(height))
# JonDoe211.84

# Printing with separators
print(first_name, last_name, age, height)
print(first_name, last_name, age, height, sep=",")
print()

# print collections
print([first_name, last_name, age, height])
# ['Jon', 'Doe', 21, 1.84]
print((first_name, last_name, age, height))
# ('Jon', 'Doe', 21, 1.84)
print()

# used starred expression to print collections
print(*[first_name, last_name, age, height])
print(*(first_name, last_name, age, height))
print(*[first_name, last_name, age, height], sep=",")
print(*(first_name, last_name, age, height), sep=",")