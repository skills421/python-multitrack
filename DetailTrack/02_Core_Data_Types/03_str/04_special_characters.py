msg = 'Embedded double quotes: "hello"'
print(msg)

msg = "Embedded single quotes: 'hello'"
print(msg)

msg = 'Embedded single quotes: \'hello\''
print(msg)

msg = "Embedded double quotes: \"hello\""
print(msg)

msg = "Escaping backslash : \\"
print(msg)

msg = "line 1\nline2"
print(msg)
print()

msg = "line 1\rline2"
print(msg)

msg = "line 1\rxx"
print(msg)

msg = 'Embedding a\t\ttab\tjust here'
print(msg)

msg = "Embedding a backspace\bjust here"
print(msg)

octal_value = int("125", 8)
msg = f"binary: {octal_value:b} octal: {octal_value:o}, dec: {octal_value:d}, hex: {octal_value:x}"
print(msg)

msg = "Embedding a curly brace {{ {0:s} }}".format("just here")
print(msg)
