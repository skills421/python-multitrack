"""
String literals in python are surrounded by either single quotation marks, or double quotation marks.
"""

msg1 = "Hello World"
msg2 = 'Hello World'

print(msg1)
print(msg2)
print()

# assigning values

name = 'Jon Doe'
print(name)
print()

# multi-line strings can be assigned using three double quotes

address = """14 High Street,
Some Town,
Some City,
UK
SW1 1AA"""

print(address)
print()

address2 = '''18 High Street,
Some Town,
Some City,
UK
SW1 1AA'''

print(address2)
print()