"""
If a collection has elements in it, then it is true
If a number is non-zero then it is true
"""

print(bool(""))
print(bool(0))
print(bool([]))
print(bool(()))
print(bool({}))
print(bool({}))
print()

print(bool("Hello"))
print(bool(3.142))
print(bool(["Ford", "Mercedes"]))
print(bool(("Ford", "Mercedes")))
print(bool({"Ford", "Mercedes"}))
print(bool({"name":"Jon Doe", "age":21}))