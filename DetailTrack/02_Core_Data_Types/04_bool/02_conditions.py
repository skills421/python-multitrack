"""
You can evaluate any expression in Python, and get one of two answers, True or False.

When you compare two values, the expression is evaluated and Python returns the Boolean answer:
"""

age = 23
drinking_age = 21

if age >= drinking_age:
    print(f"{age}: Allowed to drink")
else:
    print(f"{age}: Too young to drink")
