"""
You can evaluate any expression in Python, and get one of two answers, True or False.

When you compare two values, the expression is evaluated and Python returns the Boolean answer:
"""

x = 3 < 5
y = 3 == 5
z = 5 > 3

print(x)
print(y)
print(z)

a = True
b = False

print(a)
print(b)