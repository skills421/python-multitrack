
msg = 'Hello World'                                         # str
age = 20	                                                # int
height = 1.85	                                            # float
z = 3 + 4j	                                                # complex
student = ('Jane', 20, 1.85)                                # tuple
students = ['Jane', 'Steve', 'John', 'Josie']               # list
months = range(1, 13)                                       # range
student = {'name': 'Steve', 'age': 21, 'height': 1.85}      # dict
classrooms = { 'Einstein', 'Newton', 'Curie'}	            # set
classrooms = frozenset({ 'Einstein', 'Newton', 'Curie'})    # frozenset
married = True	                                            # bool
record = b'John03201.85'	                                # bytes
record = bytearray('Jon Doe', 'utf-8')	                    # bytearray
x = memoryview(b'John03201.85')	                            # memoryview

print(msg)
print(age)
print(height)
print(z)
print(student)
print(students)
print(months)
print(classrooms)
print(married)
print(record)
print(x)
