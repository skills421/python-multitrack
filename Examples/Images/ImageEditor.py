from PIL import Image, ImageDraw, ImageFont

# create Image object with the input image

image = Image.open('background.png')


# initialise the drawing context with
# the image object as background

draw = ImageDraw.Draw(image)

# desired size

roboto_bold = ImageFont.truetype('Roboto-Bold.ttf', size=45)

# starting position of the message

(x, y) = (50, 50)
message = "Happy Birthday!"
light_green = 'rgb(75, 255, 94)'

# draw the message on the background

draw.text((x, y), message, fill=light_green, font=roboto_bold)
(x, y) = (150, 150)
name = 'John Dunning'
white = 'rgb(255, 255, 255)'  # white color
draw.text((x, y), name, fill=white, font=roboto_bold)

# save the edited image

image.save('greeting_card.png')