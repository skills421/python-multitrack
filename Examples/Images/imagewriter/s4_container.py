from PIL import Image, ImageDraw, ImageFont

from imagewriter.s4_text_line import S4_Line


class S4_Container:

    def __init__(self, background_image: Image, style):

        self.background_image = background_image
        self.base_style = style
        self.style = style
        self.background_image_width, self.background_image_height = self.background_image.size

        self.width = 0
        self.height = 0

        self.x = 0
        self.y = 0

        self.delta_x = 0
        self.delta_y = 0

    def override_style(self, overriding_style):
        self.style = self.base_style

        if overriding_style:
            self.style = {**self.base_style, **overriding_style}

    def dpi(self):
        return self.background_image.info['dpi'][0]

    def position_container(self):
        positions = {
            'top': lambda container: (container.x, 0),
            'middle': lambda container: (container.x, int((self.background_image_height - self.height) / 2)),
            'bottom': lambda container: (container.x, self.background_image_height - self.height),
            'left': lambda container: (0, container.y),
            'center': lambda container: (int((self.background_image_width - self.width) / 2), container.y),
            'right': lambda container: (self.background_image_width - self.width, container.y)
        }

        position = self.style.get('position')

        if position:
            for placement in position:
                fn = positions.get(placement)
                if fn:
                    self.x, self.y = fn(self)

        indent = self.style.get('indent')

        if indent:
            self.x += indent[0]
            self.y += indent[1]
            self.x -= indent[2]
            self.y -= indent[3]

        up = self.style.get('up')

        if up:
            font_height = self.font.getsize(self.text)[1]
            self.y -= int(up * self.delta_y)

    def _border(self):
        border = self.style.get('border')
        if border == 'line':
            draw = ImageDraw.Draw(self.background_image)
            line_width = self.style.get('line-width') if self.style.get('line-width') else 1

            x, y, w, h = self.x, self.y, self.width, self.height

            draw.line((x, y, x+w, y), width=line_width, fill='rgb(75, 255, 94)')
            draw.line((x+w, y, x+w, y+h), width=line_width, fill='rgb(75, 255, 94)')
            draw.line((x+w, y+h, x, y+h), width=line_width, fill='rgb(75, 255, 94)')
            draw.line((x, y+h, x, y), width=line_width, fill='rgb(75, 255, 94)')
            del draw

    def draw(self):
        self._border()

    def __str__(self):

        return str({
            'image': self.background_image,
            'style': self.style,
            'image_width': self.background_image_width,
            'width': self.width,
            'height': self.height
        })


if __name__ == '__main__':
    image = Image.open('images/background.png')
    image.save('images/image_container_test.png')
    image.show()

