from PIL import Image, ImageDraw, ImageFont


class Line:

    def __init__(self, text, x, y, width):

        self.text = text
        self.x = x
        self.y = y
        self.width = width

    def draw(self, text_block, style):
        from imagewriter.s4_text import S4_Text

        font_color = style.get('font_color')
        align = style.get('align')

        if align == 'center':
            self.x = int((text_block.width - self.width) / 2)

        if align == 'right':
            self.x = text_block.width - self.width

        text_block: S4_Text = text_block
        draw = ImageDraw.Draw(text_block.background_image)
        draw.text((text_block.x + self.x, text_block.y + self.y), self.text, fill=font_color, font=text_block.font)
