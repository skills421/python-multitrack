from PIL import Image, ImageDraw, ImageFont

from imagewriter.s4_text_line import S4_Line


class Text:

    LINE_SPACE = 0.35   # line space as a percentage of font height

    def __init__(self, image: Image, text, style):

        self.image = image
        self.style = style

        self.image_width, self.image_height = self.image.size

        font_size = style.get('font_size')
        font_name = style.get('font_name')

        dpi = image.info['dpi'][0]
        point_size = int(dpi * font_size / 72)

        self.text = text
        self.font = ImageFont.truetype(font_name, point_size)

        lines = text.split('\n')
        line_count = len(lines)
        font_height = self.font.getsize(self.text)[1]

        self.width = max(self.font.getsize(line)[0] for line in lines)
        self.height = int(line_count * font_height + (line_count - 1) * font_height * Text.LINE_SPACE)

        self.x = 0
        self.y = 0

        self.lines = []

        line_offset = 0
        for line in lines:
            width = self.font.getsize(line)[0]
            text_line = S4_Line(line, 0, line_offset, width)
            self.lines.append(text_line)
            line_offset += font_height * (1 + Text.LINE_SPACE)

    def position_block(self):
        positions = {
            'top': lambda text: (text.x, 0),
            'middle': lambda text: (text.x, int((self.image_height - self.height) / 2)),
            'bottom': lambda text: (text.x, self.image_height - self.height),
            'left': lambda text: (0, text.y),
            'center': lambda text: (int((self.image_width - self.width) / 2), text.y),
            'right': lambda text: (self.image_width - self.width, text.y)
        }

        position = self.style.get('position')

        if position:
            for placement in position:
                fn = positions.get(placement)
                if fn:
                    self.x, self.y = fn(self)

        indent = self.style.get('indent')

        if indent:
            self.x += indent[0]
            self.y += indent[1]
            self.x -= indent[2]
            self.y -= indent[3]

        up = self.style.get('up')

        if up:
            font_height = self.font.getsize(self.text)[1]
            self.y -= int(up * font_height + (up - 1) * font_height * Text.LINE_SPACE)

    def draw(self):
        self.position_block()

        for text_line in self.lines:
            text_line.draw(self, self.style)

    def __str__(self):

        return str({
            'text': self.text,
            'font': self.font,
            'lines': len(self.lines),
            'width': self.width,
            'height': self.height
        })


if __name__ == '__main__':
    image = Image.open('images/background.png')
    text = 'Python\nText\nand Formatting'
    company_url = 'www.skills421.com'
    company_strapline = 'Training and Consultancy'

    style = {'font_name': 'Roboto-Regular.ttf', 'font_size': 60, 'font_color': 'rgb(75, 255, 94)',
             'position': ['middle', 'center'], 'up': 1, 'align': 'center'}

    Text(image, text, style).draw()

    style = {'font_name': 'Roboto-Regular.ttf', 'font_size': 16, 'font_color': 'rgb(75, 255, 94)',
             'position': ['bottom', 'left'], 'up': 1, 'indent': (10, 0, 0, 20), 'align': 'left'}

    Text(image, company_url, style).draw()

    style = {'font_name': 'Roboto-Regular.ttf', 'font_size': 16, 'font_color': 'rgb(145, 169, 3)',
             'position': ['bottom', 'left'], 'indent': (10, 0, 0, 10), 'align': 'left'}

    Text(image, company_strapline, style).draw()

    image.save('images/image_text_test.png')
    image.show()

