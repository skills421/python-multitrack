from PIL import Image, ImageDraw, ImageFont

from imagewriter.s4_text_line import S4_Line


class Container:

    def __init__(self, image: Image, style):

        self.image = image
        self.base_style = style
        self.style = style
        self.image_width, self.image_height = self.image.size

        self.width = 0
        self.height = 0

        self.x = 0
        self.y = 0

        self.delta_x = 0
        self.delta_y = 0
        
    def dpi(self):
        return self.image.info['dpi'][0]

    def position_container(self):
        positions = {
            'top': lambda container: (container.x, 0),
            'middle': lambda container: (container.x, int((self.image_height - self.height) / 2)),
            'bottom': lambda container: (container.x, self.image_height - self.height),
            'left': lambda container: (0, container.y),
            'center': lambda container: (int((self.image_width - self.width) / 2), container.y),
            'right': lambda container: (self.image_width - self.width, container.y)
        }

        position = self.style.get('position')

        if position:
            for placement in position:
                fn = positions.get(placement)
                if fn:
                    self.x, self.y = fn(self)

        indent = self.style.get('indent')

        if indent:
            self.x += indent[0]
            self.y += indent[1]
            self.x -= indent[2]
            self.y -= indent[3]

        up = self.style.get('up')

        if up:
            font_height = self.font.getsize(self.text)[1]
            self.y -= int(up * self.delta_y)

    def draw(self):
        self.position_block()

    def __str__(self):

        return str({
            'image': self.image,
            'style': self.style,
            'image_width': self.image_width,
            'width': self.width,
            'height': self.height
        })


if __name__ == '__main__':
    image = Image.open('images/background.png')
    image.save('images/image_container_test.png')
    image.show()

