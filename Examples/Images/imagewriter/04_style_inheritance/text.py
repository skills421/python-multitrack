from PIL import Image, ImageDraw, ImageFont

from imagewriter.s4_container import S4_Container
from imagewriter.s4_text_line import S4_Line


class Text(S4_Container):

    LINE_SPACE = 0.35   # line space as a percentage of font height

    def __init__(self, background_image: Image, base_style):
        S4_Container.__init__(self, background_image, base_style)

        self.text = None
        self.x = 0
        self.y = 0
        self.font = None
        self.lines = []
        self.text_width = 0
        self.text_height = 0
        self.width = 0
        self.height = 0

        self.delta_x = 0
        self.delta_y = 0

    def _initialise_text(self, text, overriding_style=None):
        self.override_style(overriding_style)

        self.text = text
        self.x = 0
        self.y = 0
        self.font = self._create_font()
        self.lines = []
        self.text_width = self.font.getsize(text)[0]
        self.text_height = self.font.getsize(self.text)[1]
        self.width = self.text_width
        self.height = self.text_height

        self.delta_x = self.font.getsize('X')[0]
        self.delta_y = self.font.getsize('X')[1]

        self._parse_text()

    def _parse_text(self):
        lines = self.text.split('\n')
        line_count = len(lines)

        self.width = max(self.font.getsize(line)[0] for line in lines)
        self.height = int(line_count * self.text_height + (line_count - 1) * self.text_height * Text.LINE_SPACE)

        line_offset = 0
        for line in lines:
            width = self.font.getsize(line)[0]
            text_line = S4_Line(line, 0, line_offset, width)
            self.lines.append(text_line)
            line_offset += self.text_height * (1 + Text.LINE_SPACE)

    def _create_font(self):
        font_size = self.style.get('font_size')
        font_name = self.style.get('font_name')

        dpi = self.dpi()
        point_size = int(dpi * font_size / 72)

        return ImageFont.truetype(font_name, point_size)

    def _position_text(self):
        self.position_container()

    def override_style(self, overriding_style):
        self.style = self.base_style

        if overriding_style:
            self.style = {**self.base_style, **overriding_style}

    def draw(self, text, overriding_style=None):
        self._initialise_text(text, overriding_style)
        print(self.style)
        self._position_text()

        for text_line in self.lines:
            text_line.draw(self, self.style)

    def __str__(self):

        return str({
            'text': self.text,
            'font': self.font,
            'lines': len(self.lines),
            'width': self.width,
            'height': self.height
        })


if __name__ == '__main__':
    image = Image.open('images/background.png')
    text = 'Python\nText\nand Formatting'
    company_url = 'www.skills421.com'
    company_strapline = 'Training and Consultancy'

    base_style = {'font_name': 'Roboto-Regular.ttf', 'font_size': 16, 'font_color': 'rgb(75, 255, 94)',
                  'position': ['bottom', 'left'], 'align': 'left'}

    style = {'font_size': 60, 'position': ['middle', 'center'], 'align': 'center'}

    image_text = Text(image, base_style)
    image_text.draw(text, style)

    style = {'up': 1, 'indent': (10, 0, 0, 20)}

    image_text.draw(company_url, style)

    style = {'font_color': 'rgb(145, 169, 3)', 'indent': (10, 0, 0, 10)}

    image_text.draw(company_strapline, style)

    image.save('images/image_text_test.png')
    image.show()

