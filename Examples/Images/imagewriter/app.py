from PIL import Image, ImageDraw, ImageFont

from imagewriter.s4_image import S4_Image
from imagewriter.s4_text import S4_Text

if __name__ == '__main__':
    image = Image.open('images/background.png')
    text = 'Python\nText\nand Formatting'
    company_url = 'www.skills421.com'
    company_strapline = 'Training and Consultancy'

    base_style = {'font_name': 'Roboto-Regular.ttf', 'font_size': 16, 'font_color': 'rgb(75, 255, 94)',
                  'position': ['bottom', 'left'], 'align': 'left'}

    style = {'font_size': 60, 'position': ['middle', 'center'], 'align': 'center', 'border': 'line'}

    image_text = S4_Text(image, base_style)
    image_text.draw(text, style)

    style = {'up': 1, 'indent': (10, 0, 0, 20)}

    image_text.draw(company_url, style)

    style = {'font_color': 'rgb(145, 169, 3)', 'indent': (10, 0, 0, 10)}

    image_text.draw(company_strapline, style)

    base_style = {'position': ['bottom', 'right'], 'scale': 20, 'indent': (0, 0, 10, 10)}
    style = {'border': 'line', 'line-width': 1}
    logo = Image.open('images/logo.png')

    s4_image = S4_Image(image, base_style)
    s4_image.draw(logo, style)

    image.save('images/image_text_test.png')
    image.show()
