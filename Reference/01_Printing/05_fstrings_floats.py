cost = 13456.80

# formatting floats
print(f'Cost: £{cost}p')            # Cost: £13456.8p

print(f'Cost: £{cost:f}p')          # Cost: £13456.800000p
print(f'Cost: £{cost:.2f}p')        # Cost: £13456.80p

# right and left align - pad with spaces
print(f'Cost: £{cost:10.2f}p')      # Cost: £  13456.80p
print(f'Cost: £{cost:<10.2f}p')     # Cost: £13456.80  p

# right and left align - pad with zeros
print(f'Cost: £{cost:010.2f}p')      # Cost: £0013456.80p
print(f'Cost: £{cost:<010.2f}p')     # Cost: £13456.8000p

# add comma separators
print(f'Cost: £{cost:,}p')           # Cost: £13,456.8p
print(f'Cost: £{cost:,.2f}p')        # Cost: £13,456.80p




