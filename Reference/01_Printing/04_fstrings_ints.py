age = 21

# formatting ints
print(f'Age: {age:d}yrs')

# right align - pad with spaces
print(f'Age: {age:>5d}yrs')

# left align - pad with spaces
print(f'Age: {age:<5d}yrs')

# right align - pad with zeros
print(f'Age: {age:>05d}yrs')

# left align - pad with zeros
print(f'Age: {age:<05d}yrs')
