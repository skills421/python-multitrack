name = 'Jon Doe'
age = 21
sex = 'M'
height = 1.85
weight = 182

# -- simple parameter substitution
print(f'''
{"Name"}: {name}
{"Sex"}: {sex}
{"Age"}: {age:}yrs
{"Height"}: {height}cm
{"Weight"}: {weight}lbs
''')
print()

# formatting the labels for right alignment
# :>9s
#     >  :: shift right,
#     9s ::  string with at least 9 characters
print(f'''
{"Name":>9s}: {name}
{"Sex":>9s}: {sex}
{"Age":>9s}: {age:}yrs
{"Height":>9s}: {height}cm
{"Weight":>9s}: {weight}lbs
''')
print()

# formatting the labels for right alignment
# :<9s
#     <  :: shift right,
#     9s ::  string with at least 9 characters
print(f'''
{"Name":<9s}: {name}
{"Sex":<9s}: {sex}
{"Age":<9s}: {age:}yrs
{"Height":<9s}: {height}cm
{"Weight":<9s}: {weight}lbs
''')
print()
