name = 'Jon Doe'
age = 21
sex = 'M'
height = 1.85
weight = 182

# -- single parameter substitution
print(f'Name: {name}')
print(f'Age: {age}')
print(f'Sex: {sex}')
print(f'Height: {height}')
print(f'Weight: {weight}')
print()

# -- multiple parameter substitution
print(f'{name} ({sex}) is {age} years old.')
print(f'He is {height}m tall and weighs {weight}lbs.')
print()

# -- multi-line f strings
print(f'''
  Name: {name}
   Sex: {sex}
   Age: {age}yrs
Height: {height}cm
Weight: {weight}lbs
''')
print()

