# python supports prefixed strings as follows
#
# f strings - formatting string literals
# r strings - raw strings
# b strings - byte strings
# u strings - unicode strings

name = 'Jon Doe'
age = 21
sex = 'M'
height = 1.85
weight = 182


# formatted string literals
print(f'{name} ({sex}) is {age} years old.  He is {height:.2f}m tall and weighs {weight:d}lbs ')
